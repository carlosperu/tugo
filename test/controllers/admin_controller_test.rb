require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  test "should get orders" do
    get admin_orders_url
    assert_response :success
  end

  test "should get locations" do
    get admin_locations_url
    assert_response :success
  end

  test "should get items" do
    get admin_items_url
    assert_response :success
  end

  test "should get menus" do
    get admin_menus_url
    assert_response :success
  end

  test "should get users" do
    get admin_users_url
    assert_response :success
  end

end
