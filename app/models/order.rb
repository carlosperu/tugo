class Order < ApplicationRecord
  belongs_to :location
  belongs_to :user
  has_many :order_items, dependent: :destroy
  has_many :items, through: :order_items



  STATUS = {0 => "pending", 1 => "confirmed", 2 => "received", 3 => "canceled" }

  scope :sorted, -> { order(created_at: :desc) }

  # Less than 2 minutes
  scope :recent_only, -> { where("created_at > ?", 2.minutes.ago) }

  def order_request(user, location)
    OrderMailer.order_request(user, location).deliver_now
  end

  def thank_you_order(user)
    OrderMailer.thank_you_order(user).deliver_now
  end

  def order_confirmation(user)
    OrderMailer.order_confirmation(user).deliver_now
  end

  def getStatus
    STATUS[status_id]
  end

  def confirm_status
    update(status_id: 1)
  end

end
