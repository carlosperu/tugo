class Location < ApplicationRecord
  has_many :orders, dependent: :destroy
  has_many :menus, -> { order(:position) }, dependent: :destroy

  acts_as_list

  before_save   :downcase_email

  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z]+[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i

  validates :location, presence: true, length: { maximum: 100 }
  validates :address, presence: true, length: { maximum: 100 }
  validates :city, presence: true, length: { maximum: 100 }
  validates :state, presence: true, length: { maximum: 2 }
  # TODO: Review zipcode when start including other countries
  validates :zipcode, presence: true, numericality: true,
  length: { minimum: 5, maximum: 5 }
  validates :country, presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 100 },
  format: { with: VALID_EMAIL_REGEX }
  validates :position, numericality: { minimum: 1, only_integer: true }
  validates :tax, presence: true,
  numericality: { greater_than_or_equal_to: 0, less_than_or_equal_to: 100 }

  scope :sorted, -> { order(:position) }

  scope :with_visible_menus, -> { joins(:menus).where(menus: { visible: true }) }

  private

    # Converts email to all lower-case.
    def downcase_email
      self.email.downcase!
    end
end
