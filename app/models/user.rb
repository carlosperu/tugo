class User < ApplicationRecord
  has_many :orders
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable, :confirmable

  validates :first_name, presence: true, length: { maximum: 25 }
  validates :last_name, presence: true, length: { maximum: 25 }
  validates :phone, presence: true, numericality: true,
  length: { minimum: 10, maximum: 10 }

  ROLES = { 0 => :customer, 1 => :employee, 2 => :manager, 3 => :admin }

  scope :staff, ->(user) { where("id != ? and role_id != 0 and role_id <= ?", user.id, user.role_id) }

  def role?(role)
    ROLES[role_id] == role
  end

  def getRoles
    case role_id
    when 2
      { employee: 1, manager: 2}
    when 3
      { employee: 1, manager: 2, admin: 3 }
    else
      nil
    end
  end

end
