class CategoryOption < ApplicationRecord
  belongs_to :category_section
  has_many :bag_item_choices
  has_many :bag_items, through: :bag_item_choices

  acts_as_list scope: :category_section

  validates :name, presence: true, length: { maximum: 80 }
  validates :price, presence: true, numericality: { minimum: 0 }
  validates :position, numericality: { minimum: 1, only_integer: true }

  def option
    price > 0 ? "#{self.name} +$#{price}" : self.name
  end
end
