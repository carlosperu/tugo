class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :item

  validates :quantity, presence: true, numericality: true,
  length: { minimum: 0, maximum: 50 }

end
