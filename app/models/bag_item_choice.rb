class BagItemChoice < ApplicationRecord
  belongs_to :category_option
  belongs_to :bag_item

  before_save   :add_subtotal

  private

    def add_subtotal
      self.subtotal = self.category_option ? self.bag_item.quantity * self.category_option.price : 0
    end
end
