class CategorySection < ApplicationRecord
  belongs_to :category
  has_many :category_options, -> { order(:position) }, dependent: :destroy

  acts_as_list scope: :category

  validates :position, numericality: { minimum: 1, only_integer: true }

end
