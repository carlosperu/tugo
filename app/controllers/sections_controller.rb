class SectionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :menu, only: [:new, :create]
  load_and_authorize_resource :section, through: :menu, shallow: true

  layout 'admin'

  # GET /sections/new
  def new
    # @section = Section.new(menu_id: @menu.id)
    @sections_count = Section.where(menu_id: @menu.id).count + 1
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /sections/1/edit
  def edit
    @sections_count = Section.where(menu_id: @section.menu_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /sections
  # POST /sections.json
  def create
    respond_to do |format|
      if @section.save
        format.html { redirect_to menu_url(@section.menu_id), notice: 'Section was successfully created.' }
        format.js {
          @sections = Section.where(menu_id: @section.menu_id).order(:position)
          flash[:notice] = "Section was successfully updated."
          render :index
        }
      else
        format.html { render :new }
        format.js {
          @sections_count = Section.where(menu_id: @menu.id).count + 1
          render :new
        }
      end
    end
  end

  # PATCH/PUT /sections/1
  # PATCH/PUT /sections/1.json
  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to menu_url(@section.menu_id), notice: 'Section was successfully updated.' }
        format.js {
          @sections = Section.where(menu_id: @section.menu_id).order(:position)
          flash[:notice] = "Section was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { @sections_count = Section.where(menu_id: @section.menu_id).count
          render :edit
        }
      end
    end
  end

  # DELETE /sections/1
  # DELETE /sections/1.json
  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to menu_url(@section.menu_id), notice: 'Section was successfully destroyed.' }
      format.js {
        @sections = Section.where(menu_id: @section.menu_id).order(:position)
        flash[:notice] = "Section was successfully deleted."
        render :index
      }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def section_params
      params.require(:section).permit(:menu_id, :name, :description, :position)
    end
end
