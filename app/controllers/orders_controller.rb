class OrdersController < ApplicationController
  before_action :authenticate_user!
  def index
  end

  def show
  end

  def confirm
    order = Order.find(params[:id])
    order.confirm_status
    order.order_confirmation(order.user)
    redirect_to controller: :admin, action: :orders
  end

  # Check if has order less than 2 minutes new
  def has_recent_order
    # Check if there's recent orders. If so, refresh list.
    if Order.recent_only.count > 0
      authorize! :manage, Order
      @orders = Order.sorted
      render :index
    else
      respond_to do |format|
        format.js {
          render :has_recent_order
        }
      end
    end
  end

  def new
    authorize! :create, Order
    @bag = Bag.find(session[:bag_id])
    @location = Location.find(@bag.menu.location_id)
    @order = Order.new(location_id: @location.id, subtotal: @bag.subtotal)
  end

  def create
    authorize! :create, Order
    order = Order.new(order_params)
    order.tax = (order.subtotal * order.location.tax / 100)
    order.user_id = current_user.id
    if order.save
      bag = Bag.find(session[:bag_id])
      bag.bag_items.each do |bag_item|
        item = bag_item.item
        order_item = OrderItem.new(name: item.name,
        description: item.description, quantity: bag_item.quantity,
        price: item.price, instruction: bag_item.instruction, subtotal: bag_item.subtotal)
        if order_item.save
          bag_item.bag_item_choices.each do |bag_item_choice|
            category_option = bag_item_choice.category_option
            order_item_detail = OrderItemDetail.create(name: category_option.name,
            price: category_option.price, quantity: bag_item_choice.quantity, subtotal: bag_item_choice.subtotal)
          end
        end

      end
      session[:bag_id] = nil
      order.order_request(current_user, Location.find(bag.menu.location_id))
      order.thank_you_order(current_user)
      bag.destroy
    end

  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:instruction, :location_id, :subtotal, :tax)
    end
end
