class LocationsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :location

  layout 'admin'

  # GET /locations
  # GET /locations.js
  def index
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js {
        render 'application/index', path: "locations"
      }
    end
  end

  # GET /locations/new
  # GET /locations/new.js
  def new
    @locations_count = Location.count + 1
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /locations/1/edit
  # GET /locations/1/edit.js
  def edit
    @locations_count = Location.count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /locations
  # POST /locations.js
  def create
    respond_to do |format|
      if @location.save
        format.html { redirect_to @location, notice: 'Location was successfully created.' }
        format.js {
          @locations = Location.sorted
          flash[:notice] = "Location was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js {
          @locations_count = Location.count + 1
          render :new
        }
      end
    end
  end

  # PATCH/PUT /locations/1
  # PATCH/PUT /locations/1.js
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to @location, notice: 'Location was successfully updated.' }
        format.js {
          @locations = Location.sorted
          flash[:notice] = "Location was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js {
          @locations_count = Location.count
          render :edit
        }
      end
    end
  end

  # DELETE /locations/1
  # DELETE /locations/1.js
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to locations_url, notice: 'Location was successfully deleted.' }
      format.js {
        @locations = Location.sorted
        flash[:notice] = "Location was successfully deleted."
        render :index
      }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      # params.fetch(:location, {})
      params.require(:location).permit(:location, :address, :city, :state, :zipcode, :country, :email, :email_confirmation, :position, :tax)
    end

end
