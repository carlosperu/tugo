class OrderController < ApplicationController

  layout 'customer'

  def index
  end

  def show
  end

  def new
    # Retrieve bag from current user
    if  user_signed_in?
      @bag = Bag.find_by_user_id(current_user.id)
      session[:bag_id] = @bag.id if !@bag.nil?
    end

    # If the current user does not have a bag, we look in the session
    if @bag.nil? && session[:bag_id]
      @bag = Bag.find_by_id(session[:bag_id])
      # If there's a user currently signed in, let's assign the bag to the user
      if @bag && @bag.user_id.nil? && user_signed_in?
        @bag.update(user_id: current_user.id)
      end
    end

    @locations = Location.with_visible_menus
    if @bag
      @menu = Menu.find(@bag.menu_id)
      @location = @menu.location
      @menus = @location.menus.visible
      session[:bag] = @bag
    elsif @locations && @locations.length > 0
      @location = @locations.first
      @menus = Menu.where(location_id: @location.id).sorted.visible
      @menu = @menus.first if !@menus.nil?
    end
    render :new
  end

  def menus
    authorize! :read, Menu
    @menus = Menu.where(location_id: params[:location_id]).sorted.visible
    @menu = params[:menu_id] ? Menu.find(params[:menu_id]) : @menus.first
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
  end

  def update
  end

  def edit
  end
end
