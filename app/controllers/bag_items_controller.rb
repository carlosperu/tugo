class BagItemsController < ApplicationController
  def new
    # Retrieve bag from session
    if @bag.nil?
      if user_signed_in?
        @bag = Bag.find_by_user_id(current_user.id)
      elsif session[:bag_id]
        @bag = Bag.find(session[:bag_id])
      end
    end

    # Retrieve item and menu from params
    @item = Item.find(params[:item_id])
    @menu = Menu.find(params[:menu_id])

    # If bag still nil, create one. Otherwise, just update the menu id
    if @bag.nil?
      @bag = Bag.new(menu_id: params[:menu_id])
      @bag.user_id = current_user.id if user_signed_in?
      @bag.save
    else
      @bag.update_attributes(menu_id: params[:menu_id])
    end
    @bag_item = BagItem.new(bag_id: @bag.id, item_id: @item.id)
    session[:bag_id] = @bag.id
    session[:bag_item_id] = @bag_item.id
    @bag_item_choices = []
    @item.category.category_sections.count.times do
      @bag_item_choices << BagItemChoice.new
    end
  end

  def create
    @bag = Bag.find(params[:bag_id])
    @bag_item = @bag.bag_items.build(bag_item_params)
    respond_to do |format|
      if @bag_item.save
        format.html { redirect_to menu_url(@section.menu_id),
          notice: 'The item was added successfully to the Menu.'
          render :index
        }
        format.js {
          @bag = Bag.find(session[:bag_id])
          flash[:notice] = "Item was successfully added to your Bag."
          render 'bags/index'
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  def destroy
    @bag_item = BagItem.find(params[:id])
    @bag_item.destroy
    respond_to do |format|
      format.html { redirect_to root_url, notice: 'Item was successfully removed from bag.' }
      format.js {
        @bag = Bag.find(session[:bag_id])
        flash[:notice] = "Item was successfully removed from your Bag."
        render 'bags/index'
      }
    end
  end

  private
    def bag_item_params
      params.require(:bag_item).permit(:quantity, :instruction, :bag_id, :item_id, bag_item_choices_attributes: [:category_option_id])
    end

    def bag_item_choices_params
      params.require(:bag_item).permit(:bag_item_choices_attributes)
    end
end
