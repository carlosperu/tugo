class UserController < ApplicationController
  before_action :authenticate_user!

  layout 'admin'

  def index
    authorize! :index, User
    @users = User.staff(current_user)
    render 'user/_index'
  end

  def new
    authorize! :new, User
    @user = User.new
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    authorize! :create, User
    @user = User.new(user_params)
    respond_to do |format|
      @user.skip_confirmation!
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.js {
          @users = User.staff(current_user)
          flash[:notice] = "User was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  def edit
    authorize! :edit, User
    @user = User.find(params[:id])
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    authorize! :update, User
    @user = User.find(params[:id])
    remove_password
    respond_to do |format|
      @user.skip_reconfirmation!
      if @user.update(user_params)
        format.html {
          redirect_to @user, notice: 'User was successfully updated.'
        }
        format.js {
          @users = User.staff(current_user)
          flash[:notice] = "User was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  def destroy
    authorize! :destroy, User
    @user = User.find(params[:id])
    if @user.destroy
      respond_to do |format|
        format.html { redirect_to action: :index, notice: 'Location was successfully deleted.' }
        format.js {
          @users = User.staff(current_user)
          flash[:notice] = "User was successfully deleted."
          render :index
        }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :phone, :email, :password, :password_confirmation, :role_id)
    end

    def remove_password
      params[:user].delete(:password) if params[:user][:password].blank?
      params[:user].
      delete(:password_confirmation) if params[:user][:password].blank? &&
      params[:user][:password_confirmation].blank?
    end
end
