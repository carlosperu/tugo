class CategoriesController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :category

  layout 'admin'

  # GET /categories
  # GET /categories.js
  def index
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js {
        render :index
      }
    end
  end

  # GET /categories/new
  # GET /categories/new.js
  def new
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /items/1/edit
  # GET /items/1/edit.js
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /items/1/edit
  # GET /items/1/edit.js
  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /categories
  # POST /categories.js
  def create
    respond_to do |format|
      if @category.save
        format.html { redirect_to categories_url, notice: 'Category was successfully created.' }
        format.js {
          @categories = Category.all
          flash[:notice] = "Category was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # PATCH/PUT /categories/1
  # PATCH/PUT /categories/1.js
  def update
    respond_to do |format|
      if @category.update(category_params)
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.js {
          @categories = Category.all
          flash[:notice] = "Category was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.js
  def destroy
    @category.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully deleted.' }
      format.js {
        @categories = Category.all
        flash[:notice] = "Category was successfully deleted."
        render :index
      }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:category)
    end
end
