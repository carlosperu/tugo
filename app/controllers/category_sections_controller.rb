class CategorySectionsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :category, only: [:new, :create]
  load_and_authorize_resource :category_section, through: :category, shallow: true

  layout 'admin'

  # GET /category_sections/new
  def new
    @category_sections_count = CategorySection.where(category_id: @category.id).count + 1
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /category_sections/1/edit
  def edit
    @category_sections_count = CategorySection.where(category_id: @category_section.category_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /category_sections
  # POST /category_sections.json
  def create
    respond_to do |format|
      if @category_section.save
        format.html { redirect_to category_url(@category),
          notice: 'The category was added successfully to the Menu.' }
        format.js {
          @category_sections = CategorySection.where(category_id: @category_section.category_id).order(:position)
          flash[:notice] = "Section for the category was successfully created."
          render :index
        }
      else
        @category_sections_count = CategorySection.where(category_id: @category.id).count + 1
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # PATCH/PUT /category_sections/1
  # PATCH/PUT /category_sections/1.json
  def update
    respond_to do |format|
      if @category_section.update(category_section_params)
        format.html { redirect_to category_url(@category_section.category_id),
          notice: 'Category section was successfully updated.' }
        format.js {
          @category_sections = CategorySection.where(category_id: @category_section.category_id).order(:position)
          flash[:notice] = "Section for the category was successfully updated."
          render :index
        }
      else
        @category_sections_count = CategorySection.where(category_id: @category_section.category_id).count
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /category_sections/1
  # DELETE /category_sections/1.json
  def destroy
    @category_section.destroy
    respond_to do |format|
      format.html { redirect_to category_url(@category_section.category_id), notice: 'Category section was successfully destroyed.' }
      format.js {
        @category_sections = CategorySection.where(category_id: @category_section.category_id).order(:position)
        flash[:notice] = "Section for the category was successfully deleted."
        render :index
      }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_section_params
      params.require(:category_section).
      permit(:category_id, :control_type, :required, :position, :instruction, :comment)
    end
end
