class ItemsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :item

  layout 'admin'

  # GET /items
  # GET /items.js
  def index
    @items = @items.sorted
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js {
        render :index
      }
    end
  end

  # GET /items/new
  # GET /items/new.js
  def new
    respond_to do |format|
      format.html
      format.js {
        @categories = Category.all
      }
    end
  end

  # GET /items/1/edit
  # GET /items/1/edit.js
  def edit
    respond_to do |format|
      format.html
      format.js {
        @categories = Category.all
      }
    end
  end

  # POST /items
  # POST /items.js
  def create
    respond_to do |format|
      if @item.save
        format.html { redirect_to items_url, notice: 'Item was successfully created.' }
        format.js {
          @items = Item.all.sorted
          flash[:notice] = "Item was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js { render :new }
      end
    end
  end

  # PATCH/PUT /items/1
  # PATCH/PUT /items/1.js
  def update
    respond_to do |format|
      if @item.update(item_params)
        format.html { redirect_to @item, notice: 'Item was successfully updated.' }
        format.js {
          @items = Item.all.sorted
          flash[:notice] = "Item was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js { render :edit }
      end
    end
  end

  # DELETE /items/1
  # DELETE /items/1.js
  def destroy
    @item.destroy
    respond_to do |format|
      format.html { redirect_to items_url, notice: 'Item was successfully deleted.' }
      format.js {
        @items = Item.all.sorted
        flash[:notice] = "Item was successfully deleted."
        render :index
      }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def item_params
      params.require(:item).permit(:name, :description, :price, :category_id)
    end
end
