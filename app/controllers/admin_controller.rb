class AdminController < ApplicationController
  before_action :authenticate_user!

  layout 'admin'

  def orders
    authorize! :manage, Order
    @orders = Order.sorted
    render 'orders/_index'
  end

  def locations
    authorize! :manage, Location
    @locations = Location.sorted
    render 'locations/_index'
  end

  def items
    authorize! :manage, Item
    @items = Item.sorted
    render 'items/_index'
  end

  def categories
    authorize! :manage, Category
    @categories = Category.all
    render 'categories/_index'
  end

  def menus
    authorize! :manage, Menu
      # Get sorted locations
      @locations = Location.sorted
      # Get location id if sent through the params
      location_id = params[:location_id]
      # If location provided retrieve object. Otherwise just grab first location
    if location_id
      @location = Location.find(location_id)
    else
      @location = @locations.first
    end
    @menus = Menu.in_location(@location.id).sorted
    render 'menus/_index'
  end

  def users
    authorize! :manage, User
    @users = User.staff(current_user)
    render 'user/_index'
  end
end
