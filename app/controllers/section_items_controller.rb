class SectionItemsController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :section, only: [:new, :create]
  load_and_authorize_resource :section_item, through: :section, shallow: true

  layout 'admin'

  # GET /section_items/new
  def new
    @items = Item.where.not(id: SectionItem.select(:item_id).
    where(section_id: @section.id)).sorted
    @section_items_count = SectionItem.where(section_id: @section.id).count + 1
    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /section_items/1/edit
  def edit
    @items = Item.where.not(id: SectionItem.select(:item_id).
    where(section_id: @section_item.section_id).
    where.not(item_id: @section_item.item_id)).sorted
    @section_items_count = SectionItem.where(section_id: @section_item.section_id).count
    respond_to do |format|
      format.html
      format.js
    end
  end

  # POST /section_items
  # POST /section_items.json
  def create
    respond_to do |format|
      if @section_item.save
        format.html { redirect_to menu_url(@section.menu_id),
          notice: 'The item was added successfully to the Menu.' }
        format.js {
          @section_items = SectionItem.where(section_id: @section_item.section_id).order(:position)
          flash[:notice] = "Section item was successfully created."
          render :index
        }
      else
        format.html { render :new }
        format.js {
          @items = Item.where.not(id: SectionItem.select(:item_id).
          where(section_id: @section.id)).sorted
          @section_items_count = SectionItem.where(section_id: @section.id).count + 1
          render :new
        }
      end
    end
  end

  # PATCH/PUT /section_items/1
  # PATCH/PUT /section_items/1.json
  def update
    respond_to do |format|
      if @section_item.update(section_item_params)
        format.html { redirect_to menu_url(@section_item.section.menu_id),
          notice: 'Section item was successfully updated.' }
        format.js {
          @section_items = SectionItem.where(section_id: @section_item.section_id).order(:position)
          flash[:notice] = "Section item was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js {
          @items = Item.where.not(id: SectionItem.select(:item_id).
          where(section_id: @section_item.section_id).
          where.not(item_id: @section_item.item_id)).sorted
          @section_items_count = SectionItem.where(section_id: @section_item.section_id).count
          render :edit
        }
      end
    end
  end

  # DELETE /section_items/1
  # DELETE /section_items/1.json
  def destroy
    @section_item.destroy
    respond_to do |format|
      format.html { redirect_to menu_url(@section_item.section.menu_id), notice: 'Location was successfully destroyed.' }
      format.js {
        @section_items = SectionItem.where(section_id: @section_item.section_id).order(:position)
        flash[:notice] = "Section item was successfully deleted."
        render :index
      }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def section_item_params
      params.require(:section_item).permit(:section_id, :item_id, :position)
    end
end
