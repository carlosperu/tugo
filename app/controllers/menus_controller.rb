class MenusController < ApplicationController
  before_action :authenticate_user!
  load_and_authorize_resource :location, only: [:index, :new, :create]
  load_and_authorize_resource :menu, through: :location, shallow: true

  layout 'admin'

  # GET /menus/
  # GET /menus/
  def index
    # Get sorted locations
    @locations = Location.sorted
    # Get location id if sent through the params
    location_id = params[:location_id]
    # If location provided retrieve object. Otherwise just grab first location
    if location_id
      @location = Location.find(location_id)
    else
      @location = @locations.first
    end
    @menus = Menu.in_location(@location.id)
    respond_to do |format|
      format.html {
        render '_index'
      }
      format.js {
        render :index
      }
    end
  end

  # GET /menus/1
  # GET /menus/1.json
  def show
    session[:menu_id] = @menu.id
  end

  # GET /menus/new
  def new
    @menus_count = Menu.where(location_id: @location.id).count + 1
    respond_to do |format|
      format.html
      format.js {
        @locations = Location.sorted
      }
    end
  end

  # GET /menus/1/edit
  def edit
    @menus_count = Menu.where(location_id: @menu.location_id).count
    respond_to do |format|
      format.html
      format.js {
        @locations = Location.sorted
      }
    end
  end

  # POST /menus
  # POST /menus.json
  def create
    respond_to do |format|
      if @menu.save
        format.html { redirect_to @menu, notice: 'Menu was successfully created.' }
        format.js {
            @menus = Menu.where(location_id: @menu.location_id).sorted
            @locations = Location.sorted
            flash[:notice] = "Menu was successfully created."
            render :index
        }
      else
        format.html { render :new }
        format.js {
          @menus_count = Menu.where(location_id: @menu.location_id).count + 1
          @locations = Location.sorted
          render :new
        }
      end
    end
  end

  # PATCH/PUT /menus/1
  # PATCH/PUT /menus/1.json
  def update
    respond_to do |format|
      if @menu.update(menu_params)
        format.html { redirect_to @menu, notice: 'Menu was successfully updated.' }
        format.js {
          @menus = Menu.where(location_id: @menu.location_id).sorted
          @locations = Location.sorted
          flash[:notice] = "Menu was successfully updated."
          render :index
        }
      else
        format.html { render :edit }
        format.js {
          @menus_count = Menu.where(location_id: @menu.location_id).count
          @locations = Location.sorted
          render :edit
        }
      end
    end
  end

  # DELETE /menus/1
  # DELETE /menus/1.json
  def destroy
    @menu.destroy
    respond_to do |format|
      format.html { redirect_to location_url(@menu.location_id), notice: 'Menu was successfully destroyed.' }
      format.js {
        @menus = Menu.where(location_id: @menu.location_id).sorted
        @locations = Location.all
        @menu = @menus.first
        flash[:notice] = "Menu was successfully deleted."
        render :index
      }
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def menu_params
      params.require(:menu).permit(:location_id, :label, :visible, :open, :close, :position)
    end

end
