module LocationsHelper
  COLORS = {0 => "success", 1 => "info", 2 => "warning", 3 => "danger"}
  def definePanel(count)
    COLORS[count % 4]
  end
end
