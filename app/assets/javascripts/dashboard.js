function listenToLocationsChange() {
  $("#menu_location_id").change(function() {
    $.getScript( "/locations/" + $("#menu_location_id").val() + "/menus")
      .fail(function( jqxhr, settings, exception ) {
        console.log("Something went wrong: " + exception);
    });
  });
}

function shouldRefresh() {
  if ($("#refreshOrders") && $("#refreshOrders") != undefined) {
    $.getScript("/shouldRefresh")
      .fail(function(jqxhr, settings, exception) {
          console.log("Something went wrong: " + exception);
    });
  }
}

function patchMultiLevelSideMenu() {
  var menuItem = $("#menuItems");
  var anchor = menuItem.children("a");
  var unorderedList = menuItem.children("ul");
  var caretDown = anchor.children(".fa-caret-down");
  var caretRight = anchor.children(".fa-caret-right");
  if (unorderedList.attr("aria-expanded") == undefined
      || !unorderedList.attr("aria-expanded")) {
      caretDown.hide();
      caretRight.show();
  } else {
      caretDown.show();
      caretRight.hide();
  }
  menuItem.click(function() {
      caretDown.toggle();
      caretRight.toggle();
  });
}

function initTooltip() {
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
}

// For Rails 5 (Turbolinks 5) page:load becomes turbolinks:load and will
// be even fired on initial load. So we can just do the following:
$(document).on('turbolinks:load', function() {
  listenToLocationsChange();
  patchMultiLevelSideMenu();
  initTooltip();
  // Update orders list and set to refresh in 2 minutes
  setTimeout(shouldRefresh, 120000);
});
