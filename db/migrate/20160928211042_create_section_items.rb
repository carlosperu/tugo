class CreateSectionItems < ActiveRecord::Migration[5.0]
  def change
    create_table :section_items do |t|
      t.references :section, foreign_key: true
      t.references :item, foreign_key: true
      t.integer :position

      t.timestamps
    end
  end
end
