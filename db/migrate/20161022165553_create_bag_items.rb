class CreateBagItems < ActiveRecord::Migration[5.0]
  def change
    create_table :bag_items do |t|
      t.integer :quantity, default: 1, null: false
      t.decimal :subtotal, precision: 7, scale: 2
      t.text :instruction
      t.references :bag, foreign_key: true
      t.references :item, foreign_key: true

      t.timestamps
    end
  end
end
