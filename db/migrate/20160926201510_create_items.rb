class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :name, limit: 100
      t.text :description
      t.decimal :price, precision: 6, scale: 2, default: 0
      t.references :category, foreign_key: false

      t.timestamps
    end
  end
end
