class CreateOrderItemDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :order_item_details do |t|
      t.string :name
      t.decimal :price, precision: 6, scale: 2
      t.integer :quantity
      t.decimal :subtotal, precision: 6, scale: 2

      t.timestamps
    end
  end
end
