class CreateBags < ActiveRecord::Migration[5.0]
  def change
    create_table :bags do |t|
      t.text :instruction
      t.references :user, foreign_key: true
      t.references :menu, foreign_key: true

      t.timestamps
    end
  end
end
