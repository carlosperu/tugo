class CreateCategoryOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :category_options do |t|
      t.string :name
      t.decimal :price, precision: 6, scale: 2, default: 0
      t.integer :position
      t.references :category_section, foreign_key: true

      t.timestamps
    end
  end
end
