class CreateCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :categories do |t|
      t.string :category
      t.boolean :description
      t.integer :length

      t.timestamps
    end
  end
end
