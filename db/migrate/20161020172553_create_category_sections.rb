class CreateCategorySections < ActiveRecord::Migration[5.0]
  def change
    create_table :category_sections do |t|
      t.text :instruction
      t.text :comment
      t.integer :control_type
      t.boolean :required
      t.integer :position
      t.references :category, foreign_key: true

      t.timestamps
    end
  end
end
