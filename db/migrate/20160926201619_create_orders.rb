class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.decimal :tax, precision: 5, scale: 2
      t.text :instruction
      t.integer :status_id, default: 0, null: false
      t.decimal :subtotal, precision: 8, scale: 2
      t.references :location, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
