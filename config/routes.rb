Rails.application.routes.draw do

  get 'admin/orders'
  get 'admin' => 'admin#orders'
  get 'admin/locations'
  get 'admin/items'
  get 'admin/menus'
  get 'admin/users'
  get 'admin/categories'

  devise_for :users
  devise_scope :user do
    get '/admin/login' => 'devise/sessions#new'
    get '/admin/logout' => 'devise/sessions#destroy'
    get '/login' => 'devise/sessions#new'
    get '/logout' => 'devise/sessions#destroy'
  end
  resources :user, controller: "user"
  root 'order#new'

  resources :locations  do
    resources :menus, shallow: true
    resources :orders, shallow: true
  end

  get 'shouldRefresh' => "orders#has_recent_order"
  resources :orders do
    patch 'confirm', on: :member
  end

  resources :menus  do
    resources :sections, shallow: true
  end

  resources :sections  do
    resources :section_items, shallow: true
  end

  resources :categories do
    resources :category_sections, shallow: true
  end

  resources :category_sections do
    resources :category_options, shallow: true
  end

  get 'menus/:menu_id/items/:item_id', to: 'bag_items#new'

  resources :items
  resources :bags do
    resources :bag_items, shallow: true
  end

  resources :bag_items do
    resources :bag_item_choices, shallow: true
  end
  # resources :bags

  get 'order/locations/:location_id/menus', to: 'order#menus'
  get 'order/locations/:location_id/menus/:menu_id', to: 'order#menus'
  get 'order/new'
  get 'order' => 'order#new'
  get 'takeout' => 'order#new'
end
